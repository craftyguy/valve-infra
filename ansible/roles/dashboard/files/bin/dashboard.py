#!/usr/bin/env python3
# coding: utf-8

import json
import os
import subprocess
import urllib.request
import urllib.parse
import urwid

# Custom urwid classes
import curwid
from curwid import ColorButton

from argparse import ArgumentParser

parser = ArgumentParser()
parser.add_argument('-s', '--services', nargs='+', default=[])
args = parser.parse_args()

host = "http://localhost"
networking_list = ["private", "wg0"]
services_list = args.services


def networking_data():
    widgets = []

    for nic in networking_list:
        sysfs = f"/sys/class/net/{nic}"

        if not os.path.isdir(sysfs):
            widgets.append(urwid.AttrWrap(urwid.Text(f" {nic} does not exist!"), "red"))
            continue

        with open(f"{sysfs}/operstate") as f:
            status = f.read().strip()

        ip = subprocess.getoutput(f"ip a show {nic} | grep -e 'inet\\s' |tr -s ' ' | cut -d' ' -f3")

        if status == "unknown":
            status = "???"
            color = "yellow"
        elif status == "up":
            color = "green"
        else:
            color = "brown"

        widgets.append(urwid.AttrWrap(urwid.Text(f" {nic: <8} {status: <4} {ip}"), color))

    return urwid.Pile(widgets)


def services_data():
    widgets = []

    for serv in services_list:
        status = subprocess.getoutput(f"systemctl is-active {serv}")
        expected = "active"

        if ":" in serv:
            serv, expected = serv.split(":")

        if status == "activating":
            color = "yellow"
        elif status == expected:
            color = "green"
            status = "OK"
        else:
            color = "red"

        widgets.append(urwid.AttrWrap(urwid.Text(f" {serv: <18} {status}"), color))

    return urwid.Pile(widgets)


def fetch_discover():
    discover = {}

    try:
        with urllib.request.urlopen(f'{host}/api/v1/dut/discover') as resp:
            discover = json.loads(resp.read())
    except urllib.error.URLError as e:
        return {}, f"urllib error requesting discover: {e.reason}"
    except Exception as e:
        return {}, f"Unexpected { e.__class__} in fetch_discovers: {e}"

    return discover


def fetch_pdus_duts():
    dashboard = {}
    pdus = {}
    machines = {}
    wait_for_config = []

    try:
        with urllib.request.urlopen(f'{host}/api/v1/pdus') as resp:
            pdus = json.loads(resp.read())
    except urllib.error.URLError as e:
        return {}, f"urllib error requesting pdus: {e.reason}"
    except Exception as e:
        return {}, f"Unexpected { e.__class__} in fetch_pdus_duts/pdus: {e}"

    if not pdus or ("pdus" not in pdus):
        return {}, f"error reading pdus entrypoint: {pdus}"

    if len(pdus.get("pdus")) == 0:
        return {}, "No PDUs were found"

    # Assign ports to each pdu
    # Take advantage to rename state to port-state
    for pdu, ports in pdus["pdus"].items():
        modif_ports = {}
        for p, info in ports["ports"].items():
            info = {"port-state" if k == "state" else k: v for k, v in info.items()}
            modif_ports[p] = info
        dashboard[pdu] = modif_ports

    if not dashboard:
        return {}, f"error reading pdu ports when creating the dashboard: {dashboard}"

    try:
        with urllib.request.urlopen(f'{host}/api/v1/duts') as resp:
            duts = json.loads(resp.read())
    except urllib.error.URLError as e:
        return {}, f"urllib error requesting duts: {e.reason}"
    except Exception as e:
        return {}, f"Unexpected { e.__class__} in fetch_pdus_duts/duts: {e}"

    if not duts or ("duts" not in duts):
        return {}, f"error reading duts entrypoint: {duts}"

    for mac, machine in duts["duts"].items():
        name = machine["pdu"]["name"]
        port_id = machine["pdu"]["port_id"]

        # Handle machines without PDU configuration
        if name is None or port_id is None:
            wait_for_config.append({"ip_address": machine["ip_address"],
                                    "mac_address": machine["mac_address"],
                                    })
        else:
            dashboard[name][port_id].update(machine)

    if wait_for_config:
        pdu_name = "Unknown, fix your configuration file manually"
        dashboard[pdu_name] = {}
        fake_port_id = 100
        for m in wait_for_config:
            dashboard[pdu_name][fake_port_id] = m
            fake_port_id += 1

    return dashboard, ""


def execute_request(url, data=None, method="POST"):
    if data:
        if method == "PATCH":
            data = json.dumps(data).encode()
        headers = {'Content-Type': 'application/json'}
        request = urllib.request.Request(url,
                                         data=data,
                                         headers=headers,
                                         method=method)
    else:
        request = urllib.request.Request(url, method=method)

    try:
        resp = urllib.request.urlopen(request, timeout=10)
    except urllib.error.URLError as e:
        if data is None:
            return None, f"There was some unexpected issue ({e.reason})"
        else:
            return None, f"There is a discovery process running already ({e.reason})"
    except Exception as e:
        return None, f"Unexpected { e.__class__} in execute_request: {e}"

    return resp, resp.read()


class Dashboard:
    palette = [
        ('body', 'light gray', 'black', 'standout'),
        ('header', 'dark blue', 'black', 'bold'),
        ('pdu', 'black', 'dark blue', ('standout', 'underline')),
        ('bttn_discover', 'black', 'dark green'),
        ('bttn_cancel', 'black', 'dark red'),
        ('bttn_retire', 'black', 'yellow'),
        ('buttn_activate', 'black', 'dark cyan'),
        ('buttn_info', 'black', 'light green'),
        ('buttn_default', 'black', 'dark green'),
        ('buttnf', 'white', 'dark blue', 'bold'),
        # Colors for text
        ('red', 'dark red', 'black'),
        ('brown', 'brown', 'black'),
        ('yellow', 'yellow', 'black'),
        ('green', 'dark green', 'black'),
        ('light blue', 'light blue', 'black'),
        ]

    def unhandled_input(self, key):
        if key in ('q', 'Q'):
            raise urwid.ExitMainLoop()

    def button_press(self, button, data):
        button = button.get_label()
        mac = data.get('machine').get('mac_address')
        pdu_name = data.get('pdu')
        port_id = data.get('port')

        if button == "DISCOVER":
            post_dict = {
                "pdu": data.get('pdu'),
                "port_id": data.get('port')
                }
            url = f"{host}/api/v1/dut/discover"
            post_data = json.dumps(post_dict).encode()
            resp, message = execute_request(url, post_data)
        elif (button == "CANCEL" and data.get('machine').get('port-state') == 'ON'):
            url = f"{host}/api/v1/dut/discover"
            resp, message = execute_request(url, data=None, method="DELETE")
        elif button == "DELETE MACHINE":
            url = f"{host}/api/v1/dut/{mac}"
            resp, message = execute_request(url, data=None, method="DELETE")
        elif "PORT" in button:
            if button == "UNRESERVE PORT":
                d = {"reserved": "False"}
            elif button == "RESERVE PORT":
                d = {"reserved": "True"}
            elif button == "TURN PORT OFF":
                d = {"state": "off"}
            elif button == "TURN PORT ON":
                d = {"state": "on"}

            url = f"{host}/api/v1/pdu/{pdu_name}/port/{port_id}"
            resp, message = execute_request(url, d, method="PATCH")

        else:
            mac = data.get('machine').get('mac_address')
            if button == "RETIRE":
                url = f"{host}/api/v1/dut/{mac}/retire"
            elif button == "ACTIVATE":
                url = f"{host}/api/v1/dut/{mac}/activate"
            elif button == "CANCEL":
                url = f"{host}/api/v1/dut/{mac}/cancel_job"
            elif button == "RETRAIN":
                url = f"{host}/api/v1/dut/{mac}/retrain"
            elif button == "SKIP TRAINING":
                url = f"{host}/api/v1/dut/{mac}/skip_training"
            elif button == "QUEUE QUICK CHECK":
                url = f"{host}/api/v1/dut/{mac}/quick_check"
            resp, message = execute_request(url)

        if type(message) == bytes:
            message = ''.join(map(chr, message))
        self.loop.widget.footer = urwid.AttrWrap(urwid.Text(" " + button + ": " + message), "header")

    def button_on_machine_view(self, button, data):
        self.button_press(button, data)
        self.button_close_machine_view(button, data)

    def button_close_machine_view(self, button, data):
        self.loop.widget.body = self.more_body
        self.start_alarm()

    def button_save_close_machine_view(self, button, data):
        mac = data.get('machine').get('mac_address')
        pdu_off_delay = data.get('machine').get('pdu_off_delay')

        if pdu_off_delay and pdu_off_delay != self.databox.pdu_off_delay:
            url = f"{host}/api/v1/dut/{mac}"
            d = {"pdu_off_delay": self.databox.pdu_off_delay}
            resp, message = execute_request(url, d, method="PATCH")
        # REMINDER: For future changes (e.g. label),
        # make all changes in *same* call to execute_request

        self.loop.widget.body = self.more_body
        self.start_alarm()

    def expert_mode_button(self, button, state, data):
        if not button.get_state():
            w = ColorButton("DELETE MACHINE", self.button_on_machine_view, data)
        else:
            w = urwid.Divider()

        self.expert_buttons_wrap._w = w

    def button_more(self, button, data):
        self.more_body = self.loop.widget.body
        self.stop_alarm()

        machine = data.get('machine')
        content = [urwid.Divider()]
        buttons = [ColorButton("Return", self.button_close_machine_view, data),
                   ColorButton("Save changes & return", self.button_save_close_machine_view, data),
                   urwid.Divider()]


        if machine.get('mac_address') is None:
            if machine.get('reserved'):
                buttons.append(ColorButton("UNRESERVE PORT", self.button_on_machine_view, data))
            else:
                buttons.append(ColorButton("RESERVE PORT", self.button_on_machine_view, data))
                if machine.get('port-state') == 'ON':
                    buttons.append(ColorButton("TURN PORT OFF", self.button_on_machine_view, data))
                if machine.get('port-state') == 'OFF':
                    buttons.append(ColorButton("TURN PORT ON", self.button_on_machine_view, data))

        else:
            if machine.get('ready_for_service'):
                buttons.append(ColorButton("RETRAIN", self.button_on_machine_view, data))
            else:
                buttons.append(ColorButton("SKIP TRAINING", self.button_on_machine_view, data))

            buttons.append(ColorButton("QUEUE QUICK CHECK", self.button_on_machine_view, data))

            if machine.get('state') == 'RETIRED':
                if machine.get('port-state') == 'OFF':
                    buttons.append(ColorButton("TURN PORT ON", self.button_on_machine_view, data))
                else:
                    buttons.append(ColorButton("TURN PORT OFF", self.button_on_machine_view, data))

            buttons.append(urwid.CheckBox("Expert mode", on_state_change=self.expert_mode_button, user_data=data))
            # Create a placeholder for expert buttons
            self.expert_buttons_wrap = urwid.WidgetWrap(urwid.Divider())
            buttons.append(self.expert_buttons_wrap)

        self.databox = curwid.MachinePortTable(machine)
        col_one = ('fixed', 55, self.databox)
        col_two = ('fixed', 25, urwid.Pile(buttons))
        content.append(urwid.Columns([col_one, col_two], dividechars=2))
        content.append(urwid.Divider())
        title_box = data.get('pdu') + " " + str(data.get('port'))
        self.loop.widget.body = urwid.LineBox(urwid.ListBox(urwid.SimpleListWalker(content)), title=title_box)

        # Force default focus on button "Return to main screen"
        self.loop.widget.body.original_widget.keypress((0, 30), 'right')

    def create_dashboard_body(self):

        discover = fetch_discover()
        dashboard, error_message = fetch_pdus_duts()

        if dashboard:
            # Make list to feed to ListBox
            listbox_content = []

            for pdu, ports in dashboard.items():
                listbox_content.append(urwid.Divider())
                listbox_content.append(urwid.Padding(
                    urwid.Text(("pdu", f"PDU name : {pdu}")), left=1, right=0, min_width=20))

                line = []
                for num, machine in ports.items():
                    # Return machine state or port state if there isn't a machine
                    state = machine.get('state') or machine.get('port-state')

                    line = [('fixed', 11, urwid.Text(f" Port {num}:"))]

                    if state == "TRAINING":
                        boot_loop_counts = machine.get('training').get('boot_loop_counts')
                        current_loop_count = machine.get('training').get('current_loop_count')
                        stext = f"{state} {current_loop_count}/{boot_loop_counts}"
                    elif (state == "ON" and discover and discover.get('port_id') == num and discover.get('pdu') == pdu):
                        stext = f"DISCOVERING"
                    else:
                        stext = f"{state}"
                    line.append(('fixed', 16, urwid.Text(stext)))

                    name = machine.get('full_name', "")
                    line.append(urwid.Text(name))

                    button_data = {
                        "pdu": pdu,
                        "port": num,
                        "machine": machine,
                        }

                    line.append(('fixed', 10,
                                ColorButton("MORE", self.button_more, button_data, 'buttn_info')))

                    if machine.get('reserved'):
                        line.append(('fixed', 14, urwid.Text("Reserved port")))
                    elif state == "OFF":
                        line.append(('fixed', 14,
                                     ColorButton("DISCOVER", self.button_press, button_data, 'bttn_discover')))
                    elif state in ["IDLE", "TRAINING"] and not machine.get('is_retired'):
                        line.append(('fixed', 14,
                                    ColorButton("RETIRE", self.button_press, button_data, 'bttn_retire')))
                    elif machine.get('is_retired'):
                        line.append(('fixed', 14,
                                    ColorButton("ACTIVATE", self.button_press, button_data, 'buttn_activate')))
                    # Button to cancel running job
                    elif state == "RUNNING":
                        line.append(('fixed', 14,
                                    ColorButton("CANCEL", self.button_press, button_data, 'bttn_cancel')))
                    # Button to cancel discover process
                    elif (state == "ON" and discover and discover.get('port_id') == num and discover.get('pdu') == pdu):
                        line.append(('fixed', 14,
                                    ColorButton("CANCEL", self.button_press, button_data, 'bttn_cancel')))
                    else:
                        line.append(('fixed', 14, urwid.Text("")))

                    listbox_content.append(urwid.Columns(line, min_width=10, dividechars=1))

            self.machines_box = True
        else:
            self.machines_box = False
            listbox_content = [urwid.Text(error_message)]

        net = urwid.LineBox(networking_data(), title="Networking")
        ser = urwid.LineBox(services_data(), title="Services")
        col_two = urwid.ListBox([net, ser])
        col_one = urwid.LineBox(urwid.ListBox(urwid.SimpleListWalker(listbox_content)), title="PDUs")
        columns = urwid.Columns([(col_one), ('fixed', 34, col_two)], dividechars=2)
        return columns

    def create_dashboard(self):
        header_line = " Control this dashboard with your mouse or " \
            "keys UP / DOWN / PAGE UP / PAGE DOWN / ENTER. Use Q to exit.\n"
        header = urwid.AttrWrap(urwid.Text(header_line), 'header')
        body = self.create_dashboard_body()
        footer = urwid.AttrWrap(urwid.Text(""), "header")
        frame = urwid.Frame(header=header, body=body, footer=footer)
        return frame

    def stop_alarm(self):
        self.loop.remove_alarm(self.refresh_alarm)
        self.refresh_alarm = None

    def start_alarm(self):
        self.refresh_alarm = self.loop.set_alarm_in(2, self.refresh)

    def main(self):
        self.loop = urwid.MainLoop(widget=self.create_dashboard(),
                                   palette=self.palette,
                                   unhandled_input=self.unhandled_input)
        self.start_alarm()
        self.loop.run()

    def refresh(self, loop=None, data=None):
        focus = self.loop.widget.body.widget_list[0].original_widget.get_focus_path()
        # Cleaning the CanvasCache here is necessary to remove all the extra reference to widgets
        # This will allow the garbage collector to do its job and remove old widgets.
        urwid.CanvasCache.clear()
        self.loop.widget.body = self.create_dashboard_body()
        self.start_alarm()

        if self.machines_box:
            try:
                self.loop.widget.body.widget_list[0].original_widget.set_focus_path(focus)
                # Workaround when the focus is on the second column
                if len(focus) == 2 and focus[1] == 4:
                    self.loop.widget.body.widget_list[0].original_widget.keypress((0, 1), 'right')
            # Make sure we always put the focus somewhere, in case of unknown focus
            # set the focus to the position of the first MORE button on [2,3]
            except IndexError:
                focus = [2, 3]
                self.loop.widget.body.widget_list[0].original_widget.set_focus_path(focus)


if '__main__' == __name__:
    Dashboard().main()
