#!/usr/bin/env python3
# coding: utf-8
#
# Custom urwid widgets used by the dashboard.

import urwid


class MachinePortTable(urwid.Pile):
    def __init__(self, machine):
        if machine.get('mac_address') is None and machine.get('ip_address') is None:
            list_keys = ["port-state",
                         "label",
                         "min_off_time",
                         "reserved"]
        else:
            list_keys = ["full_name",
                         "mac_address",
                         "ip_address",
                         "is_retired",
                         "ready_for_service",
                         "pdu_off_delay",
                         "state",
                         "port-state",
                         "label",
                         "min_off_time",
                         "reserved"]

        widget_list = []

        for key in list_keys:
            value = machine.get(key)
            widget_list.append(self.line_table(key, str(value)))

        if machine.get('tags'):
            widget_list.append(self.color_text("Tags"))
            value = machine.get("tags")
            for t in value:
                widget_list.append(self.line_table("", str(t)))

        if machine.get('state') == "TRAINING":
            widget_list.append(self.color_text(f"\n{'TRAINING': ^40}"))
            value = machine.get("training")
            for k, v in value.items():
                widget_list.append(self.line_table(k, str(v)))

        super(MachinePortTable, self).__init__(widget_list)

    @staticmethod
    def color_text(text):
        return urwid.AttrWrap(urwid.Text(text), 'light blue')

    def line_table(self, key, value):
        k = ('fixed', 22, urwid.AttrWrap(urwid.Text(f"{self.title_name(key):.<22}"), 'light blue'))

        if key in ['pdu_off_delay']:
            self._pdu_off_delay = urwid.AttrWrap(urwid.Edit("", str(value)), 'green', 'buttnf')
            v = ('fixed', 30, self._pdu_off_delay)
        else:
            v = ('fixed', 30, urwid.Text(value))

        return urwid.Columns([k, v], dividechars=1)

    @staticmethod
    def title_name(key):
        # Remove underscores and capitalize every word
        key = key.replace("_", " ").title()
        key = key.replace("-", " ")
        return key

    @property
    def pdu_off_delay(self):
        return self._pdu_off_delay.get_text()[0]


def ColorButton(caption, callback, data, color="buttn_default", selected_color="buttnf"):
    w = urwid.AttrWrap(urwid.Button(caption, callback, data), color, selected_color)
    return w
