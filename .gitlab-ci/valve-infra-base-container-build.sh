#!/bin/bash

set -ex

. .gitlab-ci/build_functions.sh

build() {
	buildcntr=$(buildah from --dns=none --isolation=chroot $BASE_IMAGE)
	buildmnt=$(buildah mount $buildcntr)

	cat <<EOF >$buildmnt/etc/resolv.conf
nameserver 1.1.1.1
nameserver 8.8.8.8
nameserver 4.4.4.4
EOF
	$buildah_run $buildcntr pacman -Sy --noconfirm archlinux-keyring
	$buildah_run $buildcntr pacman -Suy --noconfirm
	$buildah_run $buildcntr pacman -S --noconfirm \
		ansible-core ansible-lint yamllint \
		avahi \
		bash bash-completion \
		dnsmasq \
		gcc \
		git \
		gitlab-runner \
		htop \
		iftop \
		influx-cli \
		influxdb \
		iotop \
		minio-client \
		nano vim \
		net-snmp \
		nss-mdns \
		podman-docker \
		python \
		python-pip \
		rsync \
		speedtest-cli \
		systemd \
		tcpdump \
		wget \
		dhcpcd \
		python \
		python-netifaces \
		tmux \
		tmuxp \
		vim \
		chrony \
		fuse-overlayfs \
		influx-cli \
		influxdb \
		jq \
		minio \
		nftables \
		openssh \
		python-urwid \
		qemu-base \
		wireguard-tools \
		buildah \
		shellcheck \
		socat \
		sudo \
		yq

	# dnsmasq 2.86-1 has a bug in its signal handling, downgrade to 2.85-1 and pin
	# Reported upstream: https://lists.thekelleys.org.uk/pipermail/dnsmasq-discuss/2022q1/016133.html
	# Reported in Arch: https://bugs.archlinux.org/task/73684
	$buildah_run $buildcntr pacman --noconfirm -U https://archive.archlinux.org/packages/d/dnsmasq/dnsmasq-2.85-1-x86_64.pkg.tar.zst
	$buildah_run $buildcntr sed -i 's/^# *IgnorePkg =/IgnorePkg = dnsmasq/' /etc/pacman.conf

	# Install the docker registry by downloading a binary package, then extracting only the "registry" binary
	# NOTE: This is for amd64/x86_64
	$buildah_run $buildcntr sh -c 'wget --progres=dot:mega -O - https://github.com/distribution/distribution/releases/download/v2.8.1/registry_2.8.1_linux_amd64.tar.gz | tar xzf - -C /usr/local/bin/ registry'

	# Install all external ansible requirements here in the base container so
	# it's available for CI, etc. The ansible dir is not available in this
	# context so that's why the requirements.yml is copied into the container
	# manually
	buildah add $buildcntr ansible/requirements.yml /tmp/requirement.yml
	$buildah_run $buildcntr ansible-galaxy collection install -r /tmp/requirement.yml

	$buildah_run $buildcntr sh -c 'find /usr /etc /root -name __pycache__ -type d | xargs rm -rf'

	# gitlab-runner bundled images aren't used and just take up space
	$buildah_run $buildcntr sh -c 'rm -rf /usr/lib/gitlab-runner/helper-images/'

	$buildah_run $buildcntr sh -c 'env LC_ALL=C pacman -Qi' | awk '/^Name/{name=$3} /^Installed Size/{print $4$5, name}' | sort -h

	# pacman's clean command defaults to 'n' for cache, which makes --noconfirm do
	# exactly what we don't want (nothing)
	$buildah_run $buildcntr sh -c "yes 'y' | pacman -Scc"
}

build_and_push_container
