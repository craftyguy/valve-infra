stages:
  - prep
  - tests
  - containers
  - deploy

variables:
  GIT_SUBMODULE_STRATEGY: recursive
  FDO_UPSTREAM_REPO: mupuf/valve-infra
  PYTHON_VERSION: '3.10'

include:
  - project: 'freedesktop/ci-templates'
    ref: 34039cd573a2df832d465bc9e4c5f543571f5241
    file:
      - '/templates/arch.yml'
      - '/templates/debian.yml'
      - '/templates/alpine.yml'
      - '/templates/fedora.yml'

# Run pipelines on valve-infra CI gateways, not to waste fd.o machine time
default:
  tags:
    - CI-gateway

# Only run jobs post-merge or in merge requests
workflow:
  rules:
    - if: $CI_PIPELINE_SOURCE == "merge_request_event"
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH

#### BOOTSTRAP CONTAINERS ####

python-container:
  stage: prep
  extends:
    - .fdo.container-build@debian
  before_script:
    # Getting this on the farm runner gateways,
    #   User-selected graph driver \"vfs\" overwritten by graph driver \"overlay\" from database - delete libpod local files to resolve"
    - rm -rf /var/lib/containers/storage/libpod/*
  variables:
    # We need a build toolchain because easysnmp in unmaintained, and
    # hence has no binary wheel for us to leverage.
    FDO_DISTRIBUTION_PACKAGES: "python3 python3-pip build-essential libsnmp-dev wget"
    FDO_DISTRIBUTION_VERSION: 'bullseye'
    FDO_REPO_SUFFIX: 'python-container'
    FDO_DISTRIBUTION_TAG: '2022-02-11'
    FDO_DISTRIBUTION_EXEC: 'pip install build tox twine pycodestyle'
  rules:
    - changes:
      - .gitlab-ci.yml

.set-python-image:
  variables:
    TAG: !reference [python-container, variables, FDO_DISTRIBUTION_TAG]
  image: $CI_REGISTRY_IMAGE/python-container:$TAG
  dependencies:
    - python-container

valve-infra-base-container:
  # Minimal image for running podman and friends
  image: registry.freedesktop.org/freedesktop/ci-templates/x86_64/container-build-base:2022-09-02.0
  stage: prep
  variables:
    BASE_IMAGE: archlinux:base-devel
    FDO_DISTRIBUTION_TAG: '2022-12-16.2'
    IMAGE_NAME: $CI_REGISTRY_IMAGE/$CI_JOB_NAME:$FDO_DISTRIBUTION_TAG
    IMAGE_NAME_LATEST: $CI_REGISTRY_IMAGE/$CI_JOB_NAME:latest
  before_script:
    # Getting this on the farm runner gateways,
    #   User-selected graph driver \"vfs\" overwritten by graph driver \"overlay\" from database - delete libpod local files to resolve"
    - rm -rf /var/lib/containers/storage/libpod/*
  script: .gitlab-ci/valve-infra-base-container-build.sh
  rules:
    - changes:
      - .gitlab-ci.yml

#### TESTS ####

test ansible:
  variables:
    TAG: !reference [valve-infra-base-container, variables, FDO_DISTRIBUTION_TAG]
  image: $CI_REGISTRY_IMAGE/valve-infra-base-container:$TAG
  stage: tests
  script: |
    set -eux

    env LC_ALL=C pacman -Qi | awk '/^Name/{name=$3} /^Installed Size/{print $4$5, name}' | sort -h
    du -h -d 3 /usr /etc | sort -h
    du -h -d 1 /usr/lib/python3.*/site-packages | sort -h | tail -n 20

    # The Gitlab runner cache deliberately chmod 777's all
    # directories. This upsets ansible and there's nothing we can
    # really do about it in our repo. See
    # https://gitlab.com/gitlab-org/gitlab-runner/-/issues/4187
    chmod -R o-w ansible
    cd ansible
    ansible-lint --version
    ansible-lint -f plain -x yaml[line-length] -x yaml[commas] -x name[casing]
    ansible-playbook --syntax-check gateway.yml
  rules:
    - changes:
      - ansible/**/*

test integration:
  variables:
    TAG: !reference [valve-infra-base-container, variables, FDO_DISTRIBUTION_TAG]
    ANSIBLE_CONFIG: /builds/mupuf/valve-infra/ansible/ansible.cfg
  image: $CI_REGISTRY_IMAGE/valve-infra-base-container:$TAG
  stage: tests
  before_script: |
    pip install ./executor/client
  script: |

    shellcheck vivian/vivian

    # NOTE: local registry is skipped since starting it from within the gitlab
    # runner container doesn't work, and there's already a local registry
    # running on the runner (since runners are gateways)
    ./.gitlab-ci/run_net.sh make \
      VIVIAN_OPTS="--console-file=stdout --tests-run-ansible" \
      SKIP_LOCAL_REGISTRY=1 \
      IMAGE_NAME="10.42.0.1:8002/mupuf/valve-infra/valve-infra-container:latest" \
      vivian-integration-tests

  rules:
    - changes:
      - ansible/**/*
      - executor/**/*
      - salad/**/*
      - vivian/**/*
      - Makefile
      - .gitlab-ci.yml

  artifacts:
    when: on_failure
    paths:
      # copied from the valve-infra container on failure
      - tmp/dut-logs/

# FIXME: ipxe-boot-server should be moved to a proper Python package,
# so it can follow the pattern of the other Python services regarding
# linting, testing and building.
test ipxe-boot-server:
  extends:
    - .set-python-image
  stage: tests
  rules:
    - changes:
      - ipxe-boot-server/**/*
      - .gitlab-ci.yml
  script:
    - cd ipxe-boot-server/
    # Shouldn't we just use the setup.py to launch this, rather than calling specific tools?
    - pycodestyle --max-line-length=120 .
    - pip3 install -r requirements.txt
    - python3 -m unittest *.py

.python-test:
  extends:
    - .set-python-image
  stage: tests
  rules:
    - changes:
      - .gitlab-ci/python-container*
      - ${PACKAGE_DIR}/**/*
      - .gitlab-ci.yml
  script:
    - tox -c ${PACKAGE_DIR}/setup.cfg
    - python3 -m build ${PACKAGE_DIR}
  artifacts:
    paths:
      - ${PACKAGE_DIR}/dist/

test executor/client:
  extends:
    - .python-test
  variables:
    PACKAGE_DIR: 'executor/client'

test executor/server:
  extends:
    - .python-test
  variables:
    PACKAGE_DIR: 'executor/server'

test gfxinfo:
  extends:
    - .python-test
  variables:
    PACKAGE_DIR: 'gfxinfo'
  before_script:
    - pushd gfxinfo/src/valve_gfx_ci/gfxinfo
    # Impossible to re-use cache_database() as it would force us to install all the deps of gfxinfo just to access these URLs...
    - wget "https://gitlab.freedesktop.org/agd5f/linux/-/raw/amd-staging-drm-next/drivers/gpu/drm/amd/amdgpu/amdgpu_drv.c"
    - wget "https://gitlab.freedesktop.org/mesa/drm/-/raw/master/data/amdgpu.ids"
    - popd

test valvetraces:
  extends:
    - .python-test
  variables:
    PACKAGE_DIR: 'valvetraces'

test salad:
  extends:
    - .python-test
  variables:
    PACKAGE_DIR: 'salad'

test ansible py files:
  extends:
    - .set-python-image
  stage: tests
  rules:
    - changes:
      - ansible/**/*.py
  script:
    - find ansible/ -name '*.py' -exec  pycodestyle --max-line-length=130  "{}" \;


#### PUBLIC CONTAINERS: ####

#
# WARNING: Do not change the name of the jobs, as it will also change the name
# of the container...

mesa-trigger-container:
  stage: containers
  extends: .fdo.container-build@alpine
  before_script:
    # Getting this on the farm runner gateways,
    #   User-selected graph driver \"vfs\" overwritten by graph driver \"overlay\" from database - delete libpod local files to resolve"
    - rm -rf /var/lib/containers/storage/libpod/*
  variables:
    FDO_DISTRIBUTION_PACKAGES: 'python3 py3-pip py3-jinja2 wget'
    FDO_REPO_SUFFIX: 'mesa-trigger'
    FDO_DISTRIBUTION_TAG: '2022-12-08.1'
    FDO_DISTRIBUTION_EXEC: .gitlab-ci/${CI_JOB_NAME}.sh
  rules:
    - changes:
      - .gitlab-ci.yml

mesa-downstream-trigger-container:
  stage: containers
  extends: .fdo.container-build@fedora
  before_script:
    # Getting this on the farm runner gateways,
    #   User-selected graph driver \"vfs\" overwritten by graph driver \"overlay\" from database - delete libpod local files to resolve"
    - rm -rf /var/lib/containers/storage/libpod/*
  variables:
    FDO_BASE_IMAGE: 'quay.io/buildah/stable'
    FDO_DISTRIBUTION_PACKAGES: 'bash inotify-tools git python3-pip python3-jinja2 skopeo wget findutils'
    FDO_REPO_SUFFIX: 'mesa-downstream-trigger'
    FDO_DISTRIBUTION_TAG: '2022-11-04.1'
    FDO_DISTRIBUTION_VERSION: 'latest'
    FDO_DISTRIBUTION_EXEC: .gitlab-ci/${CI_JOB_NAME}.sh
  rules:
    - changes:
      - .gitlab-ci.yml

valve-infra-container:
  # Minimal image for running podman and friends
  image: registry.freedesktop.org/freedesktop/ci-templates/x86_64/container-build-base:2022-09-02.0
  stage: containers
  variables:
    BASE_CONTAINER_TAG: !reference [valve-infra-base-container, variables, FDO_DISTRIBUTION_TAG]
    BASE_IMAGE: $CI_REGISTRY_IMAGE/valve-infra-base-container:$BASE_CONTAINER_TAG
    FDO_DISTRIBUTION_TAG: '2022-12-19.1'
    IMAGE_NAME: $CI_REGISTRY_IMAGE/$CI_JOB_NAME:$FDO_DISTRIBUTION_TAG
    IMAGE_NAME_LATEST: $CI_REGISTRY_IMAGE/$CI_JOB_NAME:latest
  before_script:
    # Getting this on the farm runner gateways,
    #   User-selected graph driver \"vfs\" overwritten by graph driver \"overlay\" from database - delete libpod local files to resolve"
    - rm -rf /var/lib/containers/storage/libpod/*
  script: .gitlab-ci/valve-infra-container-build.sh
  rules:
    - changes:
      - .gitlab-ci.yml

machine_registration:
  # Minimal image for running podman and friends
  image: registry.freedesktop.org/freedesktop/ci-templates/x86_64/container-build-base:2022-09-02.0
  stage: containers
  variables:
    FDO_DISTRIBUTION_TAG: '2022-10-17'
    IMAGE_NAME: $CI_REGISTRY_IMAGE/$CI_JOB_NAME:$FDO_DISTRIBUTION_TAG
    IMAGE_NAME_LATEST: $CI_REGISTRY_IMAGE/$CI_JOB_NAME:latest
  before_script:
    # Getting this on the farm runner gateways,
    #   User-selected graph driver \"vfs\" overwritten by graph driver \"overlay\" from database - delete libpod local files to resolve"
    - rm -rf /var/lib/containers/storage/libpod/*
  # FIXME: Move to ci-templates when the entrypoint / working-dir changes are merged into upstream
  script: .gitlab-ci/machine-registration-container-build.sh
  rules:
    - changes:
      - .gitlab-ci.yml

telegraf-container:
  # Minimal image for running podman and friends
  image: registry.freedesktop.org/freedesktop/ci-templates/x86_64/container-build-base:2022-09-02.0
  stage: containers
  variables:
    FDO_DISTRIBUTION_TAG: 2022-04-27.1
    IMAGE_NAME: $CI_REGISTRY_IMAGE/$CI_JOB_NAME:$FDO_DISTRIBUTION_TAG
    IMAGE_NAME_LATEST: $CI_REGISTRY_IMAGE/$CI_JOB_NAME:latest
  before_script:
    # Getting this on the farm runner gateways,
    #   User-selected graph driver \"vfs\" overwritten by graph driver \"overlay\" from database - delete libpod local files to resolve"
    - rm -rf /var/lib/containers/storage/libpod/*
  script: .gitlab-ci/telegraf-container-build.sh
  rules:
    - changes:
      - .gitlab-ci.yml

valvetraces-enrollment-container:
  # Minimal image for running podman and friends
  image: registry.freedesktop.org/freedesktop/ci-templates/x86_64/container-build-base:2022-09-02.0
  stage: containers
  variables:
    FDO_DISTRIBUTION_TAG: 2022-04-12.2
    BASE_IMAGE: archlinux:base-devel
    IMAGE_NAME: $CI_REGISTRY_IMAGE/$CI_JOB_NAME:$FDO_DISTRIBUTION_TAG
    IMAGE_NAME_LATEST: $CI_REGISTRY_IMAGE/$CI_JOB_NAME:latest
  before_script:
    # Getting this on the farm runner gateways,
    #   User-selected graph driver \"vfs\" overwritten by graph driver \"overlay\" from database - delete libpod local files to resolve"
    - rm -rf /var/lib/containers/storage/libpod/*
  script: .gitlab-ci/valvetraces-enrollment-container-build.sh
  rules:
    - changes:
      - .gitlab-ci.yml

#### DEPLOYMENT JOBS ####

.python-deploy:
  extends:
    - .set-python-image
  stage: deploy
  rules:
    - if: $TWINE_PASSWORD == ""
      when: never
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH
      when: on_success
  script:
    - twine upload -u "__token__" ${PACKAGE_DIR}/dist/*
  # This will fail if the package already exists, not pleasant to
  # check ahead of time for that, so allow the job to fail
  allow_failure: true

deploy executor/client:
  extends:
    - .python-deploy
  variables:
    TWINE_PASSWORD: $PYPI_EXECUTOR_CLIENT_TOKEN
    PACKAGE_DIR: 'executor/client'
  dependencies:
    - test executor/client

deploy executor/server:
  extends:
    - .python-deploy
  variables:
    TWINE_PASSWORD: $PYPI_EXECUTOR_SERVER_TOKEN
    PACKAGE_DIR: 'executor/server'
  dependencies:
    - test executor/server

deploy gfxinfo:
  extends:
    - .python-deploy
  variables:
    TWINE_PASSWORD: $PYPI_GFXINFO_TOKEN
    PACKAGE_DIR: gfxinfo
  dependencies:
    - test gfxinfo

deploy valvetraces:
  extends:
    - .python-deploy
  variables:
    TWINE_PASSWORD: $PYPI_VALVETRACES_TOKEN
    PACKAGE_DIR: valvetraces
  dependencies:
    - test valvetraces

deploy salad:
  extends:
    - .python-deploy
  variables:
    TWINE_PASSWORD: $PYPI_SALAD_TOKEN
    PACKAGE_DIR: salad
  dependencies:
    - test salad
