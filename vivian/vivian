#!/bin/bash

# Copyright © 2021-2022 Valve Corporation
# Script to start a virtual testing harness for Valve's graphics
# infrastructure.

set -eu

RED="\e[0;31m"
YELLOW="\e[0;33m"
ENDCOLOR="\e[0m"

__host_bridge_name='vivianbr0'

__executor_port=8000
__influxd_port=8087
__salad_port=8100
__minio_port=9000
__ssh_port=60022
__ssh_id=

__disk_img=

__local_share=

__gateway_monitor_socket='gateway_monitor.sock'
__gateway_qemu_log_file='gateway_qemu.log'
__gateway_public_mac_addr='DE:AD:BE:EF:00:11'
__gateway_private_mac_addr='DE:AD:BE:EF:00:12'

__boot_mode=()

__kernel_img=''
__kernel_append=''
__ramdisk=''

__wait=false
__pause_on_cleanup=false
__cleanup_on_exit=false
__tests_run_ansible=false
__qemu_console_file=''
__current_section="vivian"

usage() {
    cat << EOF
Usage: $0 [option(s)] [command]

Options:
    --host-bridge-name <string>           Bridge interface name on host (default: "$__host_bridge_name")
    --gateway-monitor-socket <file path>  Socket for qemu monitor (default: "$__gateway_monitor_socket")
    --kernel-img <file path>              Kernel image to boot (default: none)
    --kernel-append <string>              Kernel cmdline options to append (default: none)
    --ramdisk <file path>                 Initrd to boot (default: none)
    --gateway-disk-img <file path>        Path to the gateway VM's disk, required for the "start" command,
                                          ignored for the "integration-tests" command
    --gateway-public-mac-addr <string>    Gateway public interface MAC address (default: "$__gateway_public_mac_addr")
    --gateway-private-mac-addr <string>   Gateway private interface MAC address (default: "$__gateway_private_mac_addr")
    --executor-port <port>                Port for executor (default: "$__executor_port")
    --influxd-port <port>                 Port for influxd (default: "$__influxd_port")
    --salad-port <port>                   Port for salad (default: "$__salad_port")
    --minio-port <port>                   Port for minio (default: "$__minio_port")
    --ssh-port <port>                     Port for ssh (default: "$__ssh_port")
    --ssh-id <path>                       SSH ID file to use for SSH connections (default: none)
    --local-share <path>                  Specifies the local dir to share with the gateway (default: none)
    --boot-mode <string>                  Set to "uefi" to boot gateway using UEFI (default: legacy BIOS boot)
    --wait                                Instruct qemu to wait for a telnet client connection before starting the VM
                                          (default: do not wait to start VM)
    --cleanup-on-exit                     Remove host bridge and perform other cleanup on exit (default: do not
                                          clean up)
    --pause-on-cleanup                    Pause before cleaning up (default: do not pause)
    --tests-run-ansible                   Run ansible when performing integration tests (default: do not run ansible)
    --console-file <file path>            Enable qemu output redirection to the given file (default: redirect to
                                          stdout)

Commands:
    start                                 Start gateway infra in vivian
    test-installer                        Perform gateway installer tests
    integration-tests                     Perform gateway integration testing using vivian
EOF
}


if [ -z ${1+x} ]; then
    usage
    exit 1
fi

# Use doas if available
type -P doas &>/dev/null && _sudo=doas || _sudo=sudo
[ "$(id -u)" -eq 0 ] && _sudo=

while test -n "$1"; do
    case "$1" in
        --help*|-h*)
            usage
            exit 1
            ;;
        --host-bridge-name=*)
            __host_bridge_name="${1#--host-bridge-name=}"
            ;;
        --gateway-monitor-socket=*)
            __gateway_monitor_socket="${1#--gateway-monitor-socket=}"
            ;;
        --kernel-img=*)
            __kernel_img="${1#--kernel-img=}"
                ;;
        --kernel-append=*)
            __kernel_append="${1#--kernel-append=}"
                ;;
        --ramdisk=*)
            __ramdisk="${1#--ramdisk=}"
                ;;
        --gateway-disk-img=*)
            __disk_img="${1#--gateway-disk-img=}"
                ;;
        --gateway-public-mac-addr=*)
            __gateway_public_mac_addr="${1#--gateway-public-mac-addr=}"
            ;;
        --gateway-private-mac-addr=*)
            __gateway_private_mac_addr="${1#--gateway-private-mac-addr=}"
            ;;
        --executor-port=*)
            __executor_port="${1#--executor-port=}"
            ;;
        --influxd-port=*)
            __influxd_port="${1#--influxd-port=}"
            ;;
        --local-share=*)
            __local_share="${1#--local-share=}"
            ;;
        --salad-port=*)
            __salad_port="${1#--salad-port=}"
            ;;
        --minio-port=*)
            __minio_port="${1#--minio-port=}"
            ;;
        --ssh-port=*)
            __ssh_port="${1#--ssh-port=}"
            ;;
        --ssh-id=*)
            __ssh_id="${1#--ssh-id=}"
            ;;
        --boot-mode=*)
                mode="${1#--boot-mode=}"
            if [ "$mode" = "uefi" ]; then
            __ovmf_dirs=("/usr/share/edk2-ovmf/x64" "/usr/share/OVMF")
            __ovmf=
            for d in "${__ovmf_dirs[@]}"; do
                [ -e "$d/OVMF.fd" ] && __ovmf="$d/OVMF.fd"
            done

            if [ -z "$__ovmf" ] ; then
                echo "ERROR: OVMF not found. Probably missing the edk2 ovmf packages."
                exit 1
            fi
                __boot_mode=(-drive "if=pflash,format=raw,unit=0,file=$__ovmf,readonly=on" -global "driver=cfi.pflash01,property=secure,value=off")
            else
                __boot_mode=()
            fi
            ;;
        --wait)
            __wait=true
            ;;
        --cleanup-on-exit)
            __cleanup_on_exit=true
            ;;
        --pause-on-cleanup)
            __pause_on_cleanup=true
            ;;
        --tests-run-ansible)
            __tests_run_ansible=true
            ;;
        --console-file=*)
            __qemu_console_file="${1#--console-file=}"
            ;;
        start|"test-installer"|"integration-tests")
            cmd="$1"
            break
            ;;
        *)
            echo "Error, unexpected argument $1"
            exit 1
            ;;
    esac
    shift
done

__gateway_nic_opts=(-nic "user,ipv6=off,mac=$__gateway_public_mac_addr,hostfwd=tcp::$__ssh_port-:22,hostfwd=tcp::$__executor_port-:80,hostfwd=tcp::$__influxd_port-:$__influxd_port,hostfwd=tcp::$__salad_port-:$__salad_port,hostfwd=tcp::$__minio_port-:9000,model=virtio-net-pci" -nic "bridge,br=$__host_bridge_name,mac=$__gateway_private_mac_addr,model=virtio-net-pci")
__ssh_options=(-p "$__ssh_port" -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -o LogLevel=quiet)
__scp_options=(-P "$__ssh_port" -o StrictHostKeyChecking=no -o UserKnownHostsFile=/dev/null -o LogLevel=quiet)
if [ -n "$__ssh_id" ]; then
    __ssh_options=("${__ssh_options[@]}" -i "$__ssh_id" -o IdentitiesOnly=yes)
    __scp_options=("${__scp_options[@]}" -i "$__ssh_id" -o IdentitiesOnly=yes)
fi

start_gateway() {
    [ -f "$__disk_img" ] || ( echo "$__disk_img is not a file" && exit 1 )

    if [ -n "$__kernel_img" ] || [ -n "$__ramdisk" ] || [ "$__kernel_append" ]; then
        __qemu_kernel_opts=(-kernel "$__kernel_img" -initrd "$__ramdisk")
    else
        __qemu_kernel_opts=()
    fi

    if [ -n "$__local_share" ]; then
        __qemu_mount_opts=(-virtfs "local,path=$__local_share,mount_tag=local-share,security_model=mapped-xattr,id=local-share")

        echo "

# You will be able to mount the shared local folder like so:
#
# [root@vivian ~]# mkdir -p /mnt/localshare && mount -t 9p localshare /mnt/localshare
#
"
    else
        __qemu_mount_opts=()
    fi

	__qemu_start_wait="nowait"
	if [ "$__wait" = true ]; then
		__qemu_start_wait="wait"
	fi

    set +e
    grep -q "allow $__host_bridge_name" /etc/qemu/bridge.conf 2> /dev/null
    __result=$?
    set -e
    if [ $__result -ne 0 ]; then
        if [ -f /etc/qemu/bridge.conf ] && [ $__result -ne 1 ]; then
            echo 'WARNING: You probably need to make "/etc/qemu/bridge.conf" readable!'
        else
            $_sudo mkdir -p /etc/qemu || true
            $_sudo sh -c "echo \"allow $__host_bridge_name\" >> /etc/qemu/bridge.conf"
        fi
    fi

    # WARNING: you should make sure that the
    # /usr/lib/qemu/qemu-bridge-helper (or similar path) has setuid.
    #
    # If not, you may see the following error:
    # "failed to create tun device: Operation not permitted"
    #
    # You can set the setuid with something like:
    #
    # chmod u+s /usr/lib/qemu/qemu-bridge-helper
    if [ "$__qemu_console_file" == "stdout" ]; then
        __qemu_console_opts=()
    elif [ -n "$__qemu_console_file" ]; then
        __qemu_console_opts=(-serial file:"$__qemu_console_file")
    else
        __qemu_console_opts=(-serial "telnet:localhost:4321,server,$__qemu_start_wait")
    fi

    qemu-system-x86_64 \
        -hda "$__disk_img" \
        -monitor unix:"$__gateway_monitor_socket",server,nowait \
        "${__gateway_nic_opts[@]}" \
        "${__boot_mode[@]}" \
        "${__qemu_kernel_opts[@]}" \
        "${__qemu_mount_opts[@]}" \
        "${__qemu_console_opts[@]}" \
        -nographic \
        -append "$__kernel_append console=ttyS0" \
        -m 8G \
        -cpu host \
        -enable-kvm -smp 4 &

    echo "Started gateway VM"

    if [ -n "$__qemu_console_file" ]; then
        echo "*Note: Console is being sent to: $__qemu_console_file"
    else
        echo "*Note: Console is available over telnet @ localhost:4321"
    fi
}

wait_for_ssh() {
    echo "Waiting for SSH to respond..."
    __ssh_options=("${__ssh_options[@]}" -o ConnectionAttempts=1)
    if [ -z "$__ssh_id" ]; then
        # no need to prompt for a key passphrase if there is no key set
        __ssh_options=("${__ssh_options[@]}" -o BatchMode=yes)
    fi

    local started
    started=$(date +%s)
    local timeout_sec=300
    ssh_up=false
    while [ $(( $(date +%s) - started )) -lt $timeout_sec ]; do
        if ssh "${__ssh_options[@]}" root@localhost true >/dev/null 2>&1; then
            ssh_up=true
            break
        fi
        sleep .2
    done
    if ! $ssh_up; then
        echo "error: timed out waiting for ssh connection!"
        exit 1
    fi
    echo "SSH is up..."
}

wait_for_endpoint() {
    local port=$1
    local endpoint=$2

    { local prev_shell_config=$-; set +x; } 2>/dev/null
    echo "Waiting for http://localhost:$port/$endpoint to respond..."
    local started
    started=$(date +%s)
    local timeout_sec=120
    endpoint_up=false
    while [ $(( $(date +%s) - started )) -lt $timeout_sec ]; do
        if curl -sL "http://localhost:$port/$endpoint" -o/dev/null; then
            endpoint_up=true
            break
        fi
        sleep 0.1
    done

    if ! $endpoint_up; then
        echo "error: timed out waiting for endpoint to come up!"
        exit 1
    fi

    set "-$prev_shell_config"
}

wait_for_infra() {
    wait_for_ssh

    # Container runtime weirdness? The tmp volume mount sometimes gets
    # the permission of 0700, which means reads from the nobody user
    # (dnsmasq) to the TFTP dir fail. Very strange!
    ssh "${__ssh_options[@]}" root@localhost chmod -v 755 /mnt/tmp

    # Set the farm name
    # This deletes any previous definition of the var then adds the new
    # definition to the file.
    ssh "${__ssh_options[@]}" root@localhost sed -i '/^FARM_NAME=.\/d' /mnt/permanent/config.env
    # shellcheck disable=SC2029
    ssh "${__ssh_options[@]}" root@localhost "echo FARM_NAME=\"$FARM_NAME\" >> /mnt/permanent/config.env"

    # Install firewall rules that allow access from qemu.
    # Note: must be done before wait_for_endpoint, since that requires traffic
    # from qemu being allowed
    ssh "${__ssh_options[@]}" root@localhost mkdir -p /mnt/permanent/config.d/nftables
    scp "${__scp_options[@]}" vivian/50-qemu.nft root@localhost:/mnt/permanent/config.d/nftables/
    ssh "${__ssh_options[@]}" root@localhost systemctl restart nftables

    ## Executor configuration
    wait_for_endpoint "$__executor_port" "api/v1/dut"
}

provision_infra() {
    # Initial provisioning of the MaRS DB
    EXPOSE_RUNNERS=false
    GITLAB_REGISTRATION_TOKEN="${GITLAB_REGISTRATION_TOKEN:-}"
    [ -n "${GITLAB_URL}" ] && [ -n "${GITLAB_REGISTRATION_TOKEN}" ] && EXPOSE_RUNNERS=true
    cat << EOF > vivian/__mars_db.yaml
gitlab:
  freedesktop:
    url: ${GITLAB_URL}
    registration_token: ${GITLAB_REGISTRATION_TOKEN}
    expose_runners: ${EXPOSE_RUNNERS}
    maximum_timeout: 21600
    gateway_runner:
      token: <invalid default>
      exposed: true
EOF
    scp "${__scp_options[@]}" vivian/__mars_db.yaml root@localhost:/mnt/permanent/mars_db.yaml
    rm vivian/__mars_db.yaml
}

create_host_bridge() {
    if ! ip link show "$__host_bridge_name" >/dev/null 2>&1; then
    $_sudo ip link add name "$__host_bridge_name" type bridge
    $_sudo ip link set "$__host_bridge_name" up
    $_sudo iptables -I FORWARD -m physdev --physdev-is-bridged -j ACCEPT
    else
    echo "Host bridge $__host_bridge_name already exists"
    fi
}

remove_host_bridge() {
    if ip link show "$__host_bridge_name" >/dev/null 2>&1; then
        $_sudo ip link set "$__host_bridge_name" down
        $_sudo ip link del name "$__host_bridge_name" type bridge
        # TODO: Should we get rid of this? Maybe it already existed on the system, eh...
        # iptables -I FORWARD -m physdev --physdev-is-bridged -j ACCEPT
    fi
}

cleanup() {
    echo "Performing cleanup"

	if [ "$__pause_on_cleanup" = true ]; then
        read -rp "Pausing, press enter to continue. "
	fi

    # Gracefully powerdown the gateway, I've noticed the disk can
    # become corrupted without being gentle.
    if [ -S "$__gateway_monitor_socket" ]; then
        echo "system_powerdown" | socat - unix-connect:"$__gateway_monitor_socket"
    fi
    echo "WARNING! No way to shut down the VM gracefully, be careful with disk corruption!"
    sleep 2
    rm -f "$__gateway_monitor_socket"

    # Only remove the bridge if asked for it
    if [ $__cleanup_on_exit = true ]; then
        remove_host_bridge
    fi
}

test_installer() {
    while test -n "$1"; do
        case "$1" in
            --iso=*)
                __iso="${1#--iso=}"
                ;;
            --gateway-disk-img=*)
                __gateway_disk_img="${1#--gateway-disk-img=}"
                ;;
            *)
                __passthru="$__passthru $1"
                ;;
        esac
        shift
    done

    [ -z "$__iso" ] && echo "ERROR: --iso required" && exit 1
    [ -z "$__gateway_disk_img" ] && echo "ERROR: --gateway-disk-img required" && exit 1

    if [ ! -e "$__gateway_disk_img" ]; then
        qemu-img create -f qcow2 -o size=100G "$__gateway_disk_img"
    fi

    # TODO: Allow specifying differing block device topologies
    qemu-system-x86_64 \
        -drive file="$__gateway_disk_img",if=none,id=nvm \
        -device nvme,serial=deadbeef,drive=nvm \
        -cdrom "$__iso" \
        -boot d \
        "${__gateway_nic_opts[@]}" \
        "${__boot_mode[@]}" \
        -m 4G \
        -display gtk \
        -enable-kvm \
        -serial stdio
}

function build_section_start {
    local section_params=$1
    shift
    local section_name=$1
    __current_section="$section_name"
    shift
    echo -e "\n\e[0Ksection_start:$(date +%s):$section_name$section_params\r\e[0K${YELLOW}[$(cut -d ' ' -f1 /proc/uptime)]: $*${ENDCOLOR}\n"
}

function section_start {
    # To reduce the noise related to logging, we make sure that the
    # `set -x` option is reverted before going on, but we also save what
    # was the current state so we can just restore it at the end
    { local prev_shell_config=$-; set +x; } 2>/dev/null
    build_section_start "[collapsed=true]" "$@"
    set "-$prev_shell_config"
}

function section_end {
    # To reduce the noise related to logging, we make sure that the
    # `set -x` option is reverted before going on, but we also save what
    # was the current state so we can just restore it at the end
    { local prev_shell_config=$-; set +x; } 2>/dev/null
    build_section_end "$@"
    set "-$prev_shell_config"
}

function build_section_end {
    echo -e "\e[0Ksection_end:$(date +%s):$1\r\e[0K"
    __current_section=""
}

function section_switch {
    # To reduce the noise related to logging, we make sure that the
    # `set -x` option is reverted before going on, but we also save what
    # was the current state so we can just restore it at the end
    { local prev_shell_config=$-; set +x; } 2>/dev/null
    if [ -n "$__current_section" ]
    then
        build_section_end "$__current_section"
    fi
    build_section_start "[collapsed=true]" "$@"
    set "-$prev_shell_config"
}

function uncollapsed_section_switch {
    # To reduce the noise related to logging, we make sure that the
    # `set -x` option is reverted before going on, but we also save what
    # was the current state so we can just restore it at the end
    { local prev_shell_config=$-; set +x; } 2>/dev/null
    if [ -n "$__current_section" ]
    then
        build_section_end "$__current_section"
    fi
    build_section_start "" "$@"
    set "-$prev_shell_config"
}

# $1: summary of title
# $2: message to print about failure
# $3..N: array of services get/dump logs for on the infra container (optional)
fail_test() {
    # To reduce the noise related to logging, we make sure that the
    # `set -x` option is reverted before going on, but we also save what
    # was the current state so we can just restore it at the end
    { local prev_shell_config=$-; set +x; } 2>/dev/null

    # we force the following to be not in a section
    section_end "$__current_section"

    # print the title
    local title="$1"
    echo -e "\n${RED}[$(cut -d ' ' -f1 /proc/uptime)]: ERROR: ${title}${ENDCOLOR}\n"
    set "-$prev_shell_config"

    # print the error message if one is given
    if [ $# -gt 1 ] && [ -n "$2" ]; then
        local msg="$2"
        echo "$msg"
    fi

    # dump any service log if a service is given
    if [ $# -ge 3 ] && [ -n "$3" ]; then
        local service
        for service in "${@:3}"; do
            echo "**************** Log output from $service ****************"
            ssh "${__ssh_options[@]}" root@localhost journalctl --no-pager -u "$service"
            echo "**************** End log output from $service ************"
        done
    fi

    exit 1
}

executor_base_url=http://localhost:8000
executor_base_api_url=$executor_base_url/api/v1

# Syntax: dut_api_url $machine_id $path
dut_api_url() {
    echo "$executor_base_api_url/dut/$1/$2"
}

# waits for the executor and vpdu by polling the executor's pdus endpoint,
# which requires both services to be up / functional
wait_for_executor() {
    section_switch "wait_executor_vpdu" "Waiting for executor and vpdu"
    started=$(date +%s)
    timeout_sec=60
    executor_up=false
    __out=
    while [ $(( $(date +%s) - started )) -lt $timeout_sec ]; do
        __out="$(curl --max-time 1 -sL $executor_base_api_url/pdus || true)"
        if [ -n "$(echo "$__out" | jq '.["pdus"]["VPDU"]')" ]; then
            executor_up=true
            break
        fi
        sleep 1
    done
    if ! $executor_up ; then
        fail_test "executor did not respond." "$__out" executor vpdu
    fi
}

#
# Syntax: wait_for_dut_state $machine_id $wanted_state_re $acceptable_state_re [$timeout [$polling_delay]]
#
# This functions polls $machine_id's state every $polling_delay seconds, waiting
# for the state to go from one matched by the $acceptable_state_re regex to a
# state matched by $wanted_state_re. The polling will stop after $timeout
# seconds.
#
# Finally, the function will print the current state on stdout before exiting.
#
# The following fields are optional:
#  - $timeout: defaults to 600 seconds (10 minutes)
#  - $polling_delay: defaults to 5 seconds
#
# Exit code:
#  - 0 if the state changed to $wanted_state before the hitting the timeout
#  - 1 if the state changed to an unacceptable state
#  - 2 if we hit the timeout before reaching the wanted state
#
# Example:
#   wait_for_dut_state "52:54:00:11:22:01" "IDLE" "TRAINING|RUNNING" 60 5
#
wait_for_dut_state() {
    local started mac wanted_state_re acceptable_state_re timeout polling_delay state prev_state __out
    mac=$1
    wanted_state_re=$2
    acceptable_state_re=$3
    timeout="${4:-600}"
    polling_delay="${5:-5}"

    echo "INFO: waiting for DUT state to transition from '$acceptable_state_re' to '$wanted_state_re'" 1>&2

    state=''
    started=$(date +%s)
    while [ $(( $(date +%s) - started )) -lt "$timeout" ]; do
        __out="$(curl -sL $executor_base_api_url/duts | jq ".duts[] | select(.mac_address==\"$mac\")" )"

        # If the machine does not exist, just wait for it to appear
        if [ -n "$__out" ]; then
            prev_state=$state
            state="$(echo "$__out" | jq -r '.state')"

            if [[ "$state" =~ $wanted_state_re ]]; then
                echo "SUCCESS: state changed to '$state'" 1>&2
                echo "$state"
                return 0
            elif [[ "$state" =~ $acceptable_state_re ]]; then
                # Check if the state has changed, then print it
                if [ -z "$prev_state" ]; then
                    echo "INFO: current state is '$state'" 1>&2
                elif [[ "$state" != "$prev_state" ]]; then
                    echo "INFO: state changed from '$prev_state' to '$state'" 1>&2
                fi
            else
                echo "ERROR: reached the unwanted state '$state'" 1>&2
                echo "$state"
                return 1
            fi
        fi
        sleep "$polling_delay"
    done

    echo "$state"
    return 2
}

integration_tests() {
    section_switch integration_test_start "Starting integrated testing"

    # use a special disk img just for testing, so there's no unexpected
    # data on it that might affect tests
    __disk_img="tmp/test-disk.img"

    # Remove any old test disk image so a fresh one is created each time
    [ -f "$__disk_img" ] && rm "$__disk_img"

    # Create new test disk image
    [ -d tmp/ ] || mkdir tmp
    qemu-img create -f qcow2 "$__disk_img" 20G

    if ! command -v yq &>/dev/null; then
        fail_test "no yq installed, try pip install yq"
    fi
    if ! command -v executorctl &>/dev/null; then
        fail_test "No executorctl installed, try pip install valve-gfx-ci.executor.client"
    fi
    if ! command -v mcli &>/dev/null; then
        fail_test "No mcli (minio client) installed"
    fi

    # dut log(s) copied here on error so they are available after
    mkdir -p tmp/dut-logs

    trap cleanup EXIT

    create_host_bridge
    start_gateway
    wait_for_infra
    provision_infra

    if $__tests_run_ansible; then
        section_switch "ansible_playbook" "Running ansible playbook to provision VM with current config"
        make vivian-provision
    fi

    # waiting here is necessary since sometimes the executor or vpdu are
    # restarted in the previous step, so we need to wait for them to be ready
    wait_for_executor

    # configure sergent hartman training boot count, and restart executor
    # so that config is applied
    section_switch "configure_executor" "Configuring training boot count and delay"
    ssh "${__ssh_options[@]}" root@localhost sed -i "'s/^SERGENT_HARTMAN_BOOT_COUNT=.*/SERGENT_HARTMAN_BOOT_COUNT=2/ ; s/^SERGENT_HARTMAN_QUALIFYING_BOOT_COUNT=.*/SERGENT_HARTMAN_QUALIFYING_BOOT_COUNT=2/ ; s/^SERGENT_HARTMAN_REGISTRATION_RETRIAL_DELAY=.*/SERGENT_HARTMAN_REGISTRATION_RETRIAL_DELAY=2/'" /etc/base_config.env
    ssh "${__ssh_options[@]}" root@localhost systemctl restart executor

    wait_for_executor

    section_switch "add_dut" "Adding a DUT"

    curl --max-time 10 -X POST $executor_base_api_url/dut/discover -H 'Content-Type: application/json' -d '{"pdu": "VPDU", "port_id": "1", "timeout": 150}'
    mac="52:54:00:11:22:01"

    section_switch "wait_dut_register" "Waiting for DUT to register"

    if ! wait_for_dut_state "$mac" "^TRAINING$" "^$" 150 > /dev/null; then
        scp "${__scp_options[@]}" "root@localhost:/mnt/tmp/vpdu/*.log" tmp/dut-logs/
        fail_test "did not register" "$__out" executor fdo-proxy-registry vpdu
    fi

    section_switch "wait_dut_training" "Waiting for DUT to finish training"

    # long timeout, since it can take a while to boot loop the DUT VM...
    result=0
    acceptable_states_re="^(TRAINING|RUNNING)$"
    state=$(wait_for_dut_state "$mac" "^IDLE$" "^(TRAINING|RUNNING)$" 600) || result=$?
    case "$result" in
        1)
          # ERROR: Unexpected state
          scp "${__scp_options[@]}" "root@localhost:/mnt/tmp/vpdu/*.log" tmp/dut-logs/
          # TODO: is this too aggressive? should it just print the
          # status and continue until $timeout ?
          fail_test "unexpected state" "Got the state '$state', which is not matched by '$acceptable_states_re'"
          ;;
        2)
          # Timeout
          fail_test "did not train." "$__out" executor
          ;;
    esac

    section_switch "retire_dut" "Retire the DUT"

    # Ask the executor to retire the machine
    curl --max-time 10 -X POST "$(dut_api_url $mac retire)"

    result=0
    state=$(wait_for_dut_state "$mac" "^RETIRED$" "^IDLE$" 5 1) || result=$?
    case "$result" in
        1)
            fail_test "unexpected state" "Got the unexpected state '$state'" executor
            ;;
        2)
            fail_test "the state never changed" "$__out" executor
            ;;
    esac

    section_switch "activate_dut" "Activate the DUT"

    # Ask the executor to activate the machine
    curl --max-time 10 -X POST "$(dut_api_url $mac activate)"

    result=0
    state=$(wait_for_dut_state "$mac" "^QUICK_CHECK$" "^RETIRED$" 5 1) || result=$?
    case "$result" in
        1)
            fail_test "unexpected state" "Got the unexpected state '$state'" executor
            ;;
        2)
            fail_test "the state never changed" "$__out" executor
            ;;
    esac

    section_switch "wait_for_quick_check" "Wait for the quick check to be over"

    result=0
    state=$(wait_for_dut_state "$mac" "^IDLE$" "^QUICK_CHECK$" 300 5) || result=$?
    case "$result" in
        1)
            fail_test "unexpected state" "Got the unexpected state '$state'" executor
            ;;
        2)
            fail_test "the state never changed" "$__out" executor
            ;;
    esac

    section_switch "job_share_folder" "Running a job that shares a folder"
    mkdir -p tmp/job
    echo file1 > tmp/job/file1
    echo file2 > tmp/job/file2
    success=$(echo -e "file1\nfile2\n")
    if ! executorctl -e $executor_base_url run -c 10.0.2.2 -s tmp/job/ .gitlab-ci/integration-test.yml.j2; then
        fail_test "failed to run a job" "$__out" executor
    elif ! [[ $(cat tmp/job/file3) == "$success" ]]; then
        fail_test \
            "The job did not create file3 with the expected content" "Got '$(cat tmp/job/file3)' instead of 'file1\nfile2'" \
            executor
    fi

    section_switch "test_complete" "Testing complete!"
}

case "$cmd" in
    start)
        if test -z "$__disk_img"; then
            echo "ERROR: Give a path to the disk image to use for the gateway VM"
            exit 1
        fi
        trap cleanup EXIT
        create_host_bridge
        start_gateway

        wait_for_infra
        if ! ssh "${__ssh_options[@]}" root@localhost "test -s /mnt/permanent/mars_db.yaml"; then
            provision_infra || true
        fi
        ssh "${__ssh_options[@]}" -t root@localhost env LC_ALL=en_US.utf8 tmuxp load -y .tmuxp/dashboard.yml
        ;;
    test-installer)
        create_host_bridge
        trap cleanup EXIT
        test_installer "$@"
        ;;

    integration-tests)
        integration_tests
        ;;
esac

# vim: ts=4 sw=4 et
